<?php

require __DIR__ . '/vendor/autoload.php';

$api = new \T3ko\Dpd\Api('test', 'thetu4Ee', 1495);
$api->setSandboxMode(true);

$sender = new \T3ko\Dpd\Objects\Sender(1495, 501100100, 'Jan Kowalski',
    'Bąbelkowa 1500', '02566', 'Warszawa', 'PL');
$receiver = new \T3ko\Dpd\Objects\Receiver(605600600, 'Piotr Nowak',
    'Kwiatowa 2 m. 9', '60814', 'Poznań', 'PL');

$parcel = new \T3ko\Dpd\Objects\Parcel(30, 30, 15, 1.5);
$parcel2 = new \T3ko\Dpd\Objects\Parcel(30, 30, 15, 4);
$parcel3 = new \T3ko\Dpd\Objects\Parcel(20, 30, 20, 2);

$package = new \T3ko\Dpd\Objects\Package($sender, $receiver, $parcel);
$package->addParcel($parcel2);
$package->addCODService(1200, \T3ko\Dpd\Objects\Enum\Currency::PLN());
$package->addDeclaredValueService(12000, \T3ko\Dpd\Objects\Enum\Currency::PLN());

$package2 = new \T3ko\Dpd\Objects\Package($sender, $receiver, $parcel3);

$request = \T3ko\Dpd\Request\GeneratePackageNumbersRequest::fromPackages([$package, $package2]);
$response = $api->generatePackageNumbers($request);

list($package, $package2) = $response->getPackages();
list($parcel, $parcel2) = $package->getParcels();
$parcel3 = $package2->getParcels()[0];

$request = \T3ko\Dpd\Request\GenerateLabelsRequest::fromWaybills([$parcel->getWaybill(), $parcel2->getWaybill(), $parcel3->getWaybill()]);
$response = $api->generateLabels($request);
$fp = fopen('label-' . time() . '.pdf', 'wb');
fwrite($fp, $response->getFileContent());
fclose($fp);

$request = \T3ko\Dpd\Request\GenerateProtocolRequest::fromWaybills([$parcel->getWaybill(), $parcel2->getWaybill(), $parcel3->getWaybill()]);
$response = $api->generateProtocol($request);
$fp = fopen('prtcl-' . time() . '.pdf', 'wb');
fwrite($fp, $response->getFileContent());
fclose($fp);

$request = \T3ko\Dpd\Request\GetCourierAvailabilityRequest::from('02566');
$response = $api->getCourierAvailability($request);
print_r($response);
