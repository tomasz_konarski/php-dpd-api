<?php

namespace T3ko\Dpd\Objects;

use T3ko\Dpd\Objects\Enum\Currency;
use T3ko\Dpd\Objects\Enum\GuaranteeType;
use T3ko\Dpd\Objects\Enum\PayerType;
use T3ko\Dpd\Objects\Enum\SelfCollectionReceiver;

class Package
{
    /**
     * @var Parcel[]
     */
    private $parcels;

    /**
     * @var Sender
     */
    private $sender;

    /**
     * @var Receiver
     */
    private $receiver;

    /**
     * @var PayerType
     */
    private $payerType;

    /**
     * @var Services
     */
    private $services;

    /**
     * @var string
     */
    private $ref1;

    /**
     * @var string
     */
    private $ref2;

    /**
     * @var string
     */
    private $ref3;

    /**
     * @var int
     */
    private $thirdPartyFid;

    /**
     * Package constructor.
     *
     * @param Sender   $sender
     * @param Receiver $receiver
     * @param Parcel   $parcel
     */
    public function __construct(Sender $sender, Receiver $receiver, Parcel ...$parcel)
    {
        $this->sender = $sender;
        $this->receiver = $receiver;
        $this->payerType = PayerType::SENDER();
        $this->parcels = $parcel;
        $this->services = new Services();
    }

    /**
     * @return Parcel[]
     */
    public function getParcels()
    {
        return $this->parcels;
    }

    /**
     * @return Sender
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @return Receiver
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * @return PayerType
     */
    public function getPayerType()
    {
        return $this->payerType;
    }

    /**
     * @return Services
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @return string
     */
    public function getRef1()
    {
        return $this->ref1;
    }

    /**
     * @return string
     */
    public function getRef2()
    {
        return $this->ref2;
    }

    /**
     * @return string
     */
    public function getRef3()
    {
        return $this->ref3;
    }

    /**
     * @return int
     */
    public function getThirdPartyFid()
    {
        return $this->thirdPartyFid;
    }

    /**
     * @param PayerType $payerType
     *
     * @return Package
     */
    public function setPayerType(PayerType $payerType) 
    {
        $this->payerType = $payerType;

        return $this;
    }

    /**
     * @param string $ref1
     *
     * @return Package
     */
    public function setRef1($ref1) 
    {
        $this->ref1 = $ref1;

        return $this;
    }

    /**
     * @param string $ref2
     *
     * @return Package
     */
    public function setRef2($ref2) 
    {
        $this->ref2 = $ref2;

        return $this;
    }

    /**
     * @param string $ref3
     *
     * @return Package
     */
    public function setRef3($ref3) 
    {
        $this->ref3 = $ref3;

        return $this;
    }

    /**
     * @param int $thirdPartyFid
     *
     * @return Package
     */
    public function setThirdPartyFid($thirdPartyFid) 
    {
        $this->thirdPartyFid = $thirdPartyFid;

        return $this;
    }

    /**
     * @param Parcel $parcel
     *
     * @return Package
     */
    public function addParcel(Parcel $parcel)
    {
        $this->parcels[] = $parcel;

        return $this;
    }

    public function addCarryInService()
    {
        $this->services->setCarryIn(true);

        return $this;
    }

    public function addCODService($amount, Currency $currency)
    {
        $this->services->setCodAmount($amount);
        $this->services->setCodCurrency($currency);

        return $this;
    }

    public function addCUDService()
    {
        $this->services->setCud(true);

        return $this;
    }

    public function addDeclaredValueService($amount, Currency $currency)
    {
        $this->services->setDeclaredValueAmount($amount);
        $this->services->setDeclaredValueCurrency($currency);

        return $this;
    }

    public function addDedicatedDeliveryService()
    {
        $this->services->setDedicatedDelivery(true);

        return $this;
    }

    public function addDOXService()
    {
        $this->services->setDox(true);

        return $this;
    }

    public function addDutyService($amount, Currency $currency)
    {
        $this->services->setDutyAmount($amount);
        $this->services->setDutyCurrency($currency);

        return $this;
    }

    public function addGuaranteeService(GuaranteeType $type,$value = null)
    {
        $this->services->setGuaranteeType($type);
        $this->services->setGuaranteeValue($value);

        return $this;
    }

    public function addInPersService()
    {
        $this->services->setInPers(true);

        return $this;
    }

    public function addPalletService()
    {
        $this->services->setPallet(true);

        return $this;
    }

    public function addPrivPersService()
    {
        $this->services->setPrivPers(true);

        return $this;
    }

    public function addRODService()
    {
        $this->services->setRod(true);

        return $this;
    }

    public function addSelfColService(SelfCollectionReceiver $receiver)
    {
        $this->services->setSelfCollection($receiver);

        return $this;
    }

    public function addTiresService()
    {
        $this->services->setTires(true);

        return $this;
    }

    public function addTiresExportService()
    {
        $this->services->setTiresExport(true);

        return $this;
    }
}
