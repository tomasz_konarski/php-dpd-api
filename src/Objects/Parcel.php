<?php

namespace T3ko\Dpd\Objects;

class Parcel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $sizeX;

    /**
     * @var int
     */
    private $sizeY;

    /**
     * @var int
     */
    private $sizeZ;

    /**
     * @var float
     */
    private $weight;

    /**
     * @var string
     */
    private $contents;

    /**
     * @var string
     */
    private $reference;

    /**
     * Parcel constructor.
     *
     * @param int    $sizeX
     * @param int    $sizeY
     * @param int    $sizeZ
     * @param float  $weight
     * @param string $reference
     * @param string $contents
     */
    public function __construct($sizeX, $sizeY, $sizeZ, $weight, $reference = null, $contents = null)
    {
        $this->sizeX = $sizeX;
        $this->sizeY = $sizeY;
        $this->sizeZ = $sizeZ;
        $this->weight = $weight;
        $this->reference = $reference;
        $this->contents = $contents;
    }

    /**
     * @return int
     */
    public function getSizeX()
    {
        return $this->sizeX;
    }

    /**
     * @return int
     */
    public function getSizeY()
    {
        return $this->sizeY;
    }

    /**
     * @return int
     */
    public function getSizeZ()
    {
        return $this->sizeZ;
    }

    /**
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @return string
     */
    public function getContents()
    {
        return $this->contents;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }
}
