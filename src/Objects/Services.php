<?php

namespace T3ko\Dpd\Objects;

use T3ko\Dpd\Objects\Enum\Currency;
use T3ko\Dpd\Objects\Enum\GuaranteeType;
use T3ko\Dpd\Objects\Enum\SelfCollectionReceiver;

class Services
{
    /**
     * @var bool
     */
    private $carryIn = false;

    /**
     * @var int
     */
    private $codAmount;

    /**
     * @var Currency
     */
    private $codCurrency;

    /**
     * @var bool
     */
    private $cud = false;

    /**
     * @var int
     */
    private $declaredValueAmount;

    /**
     * @var Currency
     */
    private $declaredValueCurrency;

    /**
     * @var bool
     */
    private $dedicatedDelivery = false;

    /**
     * @var bool
     */
    private $documentsInternational = false;

    /**
     * @var bool
     */
    private $dox = false;

    /**
     * @var bool
     */
    private $dpdExpress = false;

    /**
     * @var string
     */
    private $dpdPickupPudo;
    /**
     * @var int
     */
    private $dutyAmount;

    /**
     * @var Currency
     */
    private $dutyCurrency;

    /**
     * @var GuaranteeType
     */
    private $guaranteeType;

    /**
     * @var string
     */
    private $guaranteeValue;

    /**
     * @var bool
     */
    private $inPers = false;

    /**
     * @var bool
     */
    private $pallet = false;

    /**
     * @var bool
     */
    private $privPers = false;

    /**
     * @var bool
     */
    private $rod = false;

    /**
     * @var SelfCollectionReceiver
     */
    private $selfCollection;

    /**
     * @var bool
     */
    private $tires = false;

    /**
     * @var bool
     */
    private $tiresExport = false;

    /**
     * @return bool
     */
    public function isCarryIn()
    {
        return $this->carryIn;
    }

    /**
     * @param bool $carryIn
     *
     * @return Services
     */
    public function setCarryIn($carryIn)
    {
        $this->carryIn = $carryIn;

        return $this;
    }

    /**
     * @return int
     */
    public function getCodAmount()
    {
        return $this->codAmount;
    }

    /**
     * @param int $codAmount
     *
     * @return Services
     */
    public function setCodAmount($codAmount) 
    {
        $this->codAmount = $codAmount;

        return $this;
    }

    /**
     * @return Currency
     */
    public function getCodCurrency()
    {
        return $this->codCurrency;
    }

    /**
     * @param Currency $codCurrency
     *
     * @return Services
     */
    public function setCodCurrency(Currency $codCurrency) 
    {
        $this->codCurrency = $codCurrency;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCud()
    {
        return $this->cud;
    }

    /**
     * @param bool $cud
     *
     * @return Services
     */
    public function setCud($cud)
    {
        $this->cud = $cud;

        return $this;
    }

    /**
     * @return int
     */
    public function getDeclaredValueAmount()
    {
        return $this->declaredValueAmount;
    }

    /**
     * @param int $declaredValueAmount
     *
     * @return Services
     */
    public function setDeclaredValueAmount($declaredValueAmount) 
    {
        $this->declaredValueAmount = $declaredValueAmount;

        return $this;
    }

    /**
     * @return Currency
     */
    public function getDeclaredValueCurrency()
    {
        return $this->declaredValueCurrency;
    }

    /**
     * @param Currency $declaredValueCurrency
     *
     * @return Services
     */
    public function setDeclaredValueCurrency(Currency $declaredValueCurrency) 
    {
        $this->declaredValueCurrency = $declaredValueCurrency;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDedicatedDelivery()
    {
        return $this->dedicatedDelivery;
    }

    /**
     * @param bool $dedicatedDelivery
     *
     * @return Services
     */
    public function setDedicatedDelivery($dedicatedDelivery)
    {
        $this->dedicatedDelivery = $dedicatedDelivery;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDox()
    {
        return $this->dox;
    }

    /**
     * @param bool $dox
     *
     * @return Services
     */
    public function setDox($dox)
    {
        $this->dox = $dox;

        return $this;
    }

    /**
     * @return int
     */
    public function getDutyAmount()
    {
        return $this->dutyAmount;
    }

    /**
     * @param int $dutyAmount
     *
     * @return Services
     */
    public function setDutyAmount($dutyAmount) 
    {
        $this->dutyAmount = $dutyAmount;

        return $this;
    }

    /**
     * @return Currency
     */
    public function getDutyCurrency()
    {
        return $this->dutyCurrency;
    }

    /**
     * @param Currency $dutyCurrency
     *
     * @return Services
     */
    public function setDutyCurrency(Currency $dutyCurrency) 
    {
        $this->dutyCurrency = $dutyCurrency;

        return $this;
    }

    /**
     * @return GuaranteeType
     */
    public function getGuaranteeType()
    {
        return $this->guaranteeType;
    }

    /**
     * @param GuaranteeType $guaranteeType
     *
     * @return Services
     */
    public function setGuaranteeType(GuaranteeType $guaranteeType) 
    {
        $this->guaranteeType = $guaranteeType;

        return $this;
    }

    /**
     * @return string
     */
    public function getGuaranteeValue()
    {
        return $this->guaranteeValue;
    }

    /**
     * @param string $guaranteeValue
     *
     * @return Services
     */
    public function setGuaranteeValue($guaranteeValue) 
    {
        $this->guaranteeValue = $guaranteeValue;

        return $this;
    }

    /**
     * @return bool
     */
    public function isInPers() 
    {
        return $this->inPers;
    }

    /**
     * @param bool $inPers
     *
     * @return Services
     */
    public function setInPers($inPers)
    {
        $this->inPers = $inPers;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPallet()
    {
        return $this->pallet;
    }

    /**
     * @param bool $pallet
     *
     * @return Services
     */
    public function setPallet($pallet)
    {
        $this->pallet = $pallet;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPrivPers()
    {
        return $this->privPers;
    }

    /**
     * @param bool $privPers
     *
     * @return Services
     */
    public function setPrivPers($privPers)
    {
        $this->privPers = $privPers;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRod()
    {
        return $this->rod;
    }

    /**
     * @param bool $rod
     *
     * @return Services
     */
    public function setRod($rod)
    {
        $this->rod = $rod;

        return $this;
    }

    /**
     * @return SelfCollectionReceiver
     */
    public function getSelfCollection()
    {
        return $this->selfCollection;
    }

    /**
     * @param SelfCollectionReceiver $selfCollection
     *
     * @return Services
     */
    public function setSelfCollection(SelfCollectionReceiver $selfCollection) 
    {
        $this->selfCollection = $selfCollection;

        return $this;
    }

    /**
     * @return bool
     */
    public function isTires()
    {
        return $this->tires;
    }

    /**
     * @param bool $tires
     *
     * @return Services
     */
    public function setTires($tires)
    {
        $this->tires = $tires;

        return $this;
    }

    /**
     * @return bool
     */
    public function isTiresExport()
    {
        return $this->tiresExport;
    }

    /**
     * @param bool $tiresExport
     *
     * @return Services
     */
    public function setTiresExport($tiresExport)
    {
        $this->tiresExport = $tiresExport;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDocumentsInternational()
    {
        return $this->documentsInternational;
    }

    /**
     * @param bool $documentsInternational
     */
    public function setDocumentsInternational($documentsInternational)
    {
        $this->documentsInternational = $documentsInternational;
    }

    /**
     * @return bool
     */
    public function isDpdExpress()
    {
        return $this->dpdExpress;
    }

    /**
     * @param bool $dpdExpress
     */
    public function setDpdExpress($dpdExpress)
    {
        $this->dpdExpress = $dpdExpress;
    }

    /**
     * @return string
     */
    public function getDpdPickupPudo()
    {
        return $this->dpdPickupPudo;
    }

    /**
     * @param string $dpdPickupPudo
     */
    public function setDpdPickupPudo($dpdPickupPudo)
    {
        $this->dpdPickupPudo = $dpdPickupPudo;
    }
}
