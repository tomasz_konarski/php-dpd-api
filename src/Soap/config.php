<?php

use Phpro\SoapClient\CodeGenerator\Assembler;
use Phpro\SoapClient\CodeGenerator\Config\Config;
use Phpro\SoapClient\CodeGenerator\Rules;

return Config::create()
    ->setWsdl('https://dpdservicesdemo.dpd.com.pl/DPDPackageObjServicesService/DPDPackageObjServices?wsdl')
    ->setTypeDestination('src/Soap/Types')
    ->setTypeNamespace('T3ko\\Dpd\\Soap\\Types')
    ->setClientDestination('src/Soap/Client')
    ->setClientNamespace('T3ko\\Dpd\\Soap\\Client')
    ->setClientName('Client')
    ->addSoapOption('features', SOAP_SINGLE_ELEMENT_ARRAYS)
    ->addRule(new Rules\AssembleRule(
        new Assembler\GetterAssembler(
            Assembler\GetterAssemblerOptions::create()
                ->withReturnType(true)
                ->withBoolGetters(true)
        )
    ))
    ->addRule(new Rules\AssembleRule(
        new Assembler\FluentSetterAssembler(
            Assembler\FluentSetterAssemblerOptions::create()
                ->withReturnType(true)
        )
    ))
    ->addRule(new Rules\TypenameMatchesRule(
        new Rules\AssembleRule(new Assembler\RequestAssembler()),
        '/Request$/'
    ))
    ->addRule(new Rules\TypenameMatchesRule(
        new Rules\AssembleRule(new Assembler\ResultAssembler()),
        '/Response$/'
    ))
    ;
