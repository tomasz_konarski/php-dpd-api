<?php

namespace T3ko\Dpd\Soap\Types;

class DpdServicesParamsV2
{
    /**
     * @var PolicyDSPEnumV2
     */
    private $policy;

    /**
     * @var SessionDSPV2
     */
    private $session;

    /**
     * @var PickupAddressDSPV2
     */
    private $pickupAddress;

    /**
     * @var DeliveryDestinations
     */
    private $deliveryDestinations;

    /**
     * @var bool
     */
    private $genProtForNonMatching;

    /**
     * @return PolicyDSPEnumV2
     */
    public function getPolicy() 
    {
        return $this->policy;
    }

    /**
     * @param PolicyDSPEnumV2 $policy
     *
     * @return $this
     */
    public function setPolicy(PolicyDSPEnumV2 $policy) 
    {
        $this->policy = $policy;

        return $this;
    }

    /**
     * @return SessionDSPV2
     */
    public function getSession() 
    {
        return $this->session;
    }

    /**
     * @param SessionDSPV2 $session
     *
     * @return $this
     */
    public function setSession(SessionDSPV2 $session) 
    {
        $this->session = $session;

        return $this;
    }

    /**
     * @return PickupAddressDSPV2
     */
    public function getPickupAddress() 
    {
        return $this->pickupAddress;
    }

    /**
     * @param PickupAddressDSPV2 $pickupAddress
     *
     * @return $this
     */
    public function setPickupAddress(PickupAddressDSPV2 $pickupAddress) 
    {
        $this->pickupAddress = $pickupAddress;

        return $this;
    }

    /**
     * @return DeliveryDestinations
     */
    public function getDeliveryDestinations() 
    {
        return $this->deliveryDestinations;
    }

    /**
     * @param DeliveryDestinations $deliveryDestinations
     *
     * @return $this
     */
    public function setDeliveryDestinations(DeliveryDestinations $deliveryDestinations) 
    {
        $this->deliveryDestinations = $deliveryDestinations;

        return $this;
    }

    /**
     * @return bool
     */
    public function isGenProtForNonMatching() 
    {
        return $this->genProtForNonMatching;
    }

    /**
     * @param bool $genProtForNonMatching
     *
     * @return $this
     */
    public function setGenProtForNonMatching($genProtForNonMatching) 
    {
        $this->genProtForNonMatching = $genProtForNonMatching;

        return $this;
    }
}
