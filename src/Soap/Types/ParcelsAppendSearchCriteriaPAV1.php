<?php

namespace T3ko\Dpd\Soap\Types;

class ParcelsAppendSearchCriteriaPAV1
{

    /**
     * @var int
     */
    private $packageId;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var string
     */
    private $waybill;

    /**
     * @return int
     */
    public function getPackageId() 
    {
        return $this->packageId;
    }

    /**
     * @param int $packageId
     * @return $this
     */
    public function setPackageId($packageId) 
    {
        $this->packageId = $packageId;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference() 
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return $this
     */
    public function setReference($reference) 
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return string
     */
    public function getWaybill() 
    {
        return $this->waybill;
    }

    /**
     * @param string $waybill
     * @return $this
     */
    public function setWaybill($waybill) 
    {
        $this->waybill = $waybill;
        return $this;
    }


}

