<?php

namespace T3ko\Dpd\Soap\Types;

class ParcelPGRV2
{

    /**
     * @var string
     */
    private $Status;

    /**
     * @var int
     */
    private $ParcelId;

    /**
     * @var string
     */
    private $Reference;

    /**
     * @var string
     */
    private $Waybill;

    private $ValidationDetails;

    /**
     * @return string
     */
    public function getStatus() 
    {
        return $this->Status;
    }

    /**
     * @param string $Status
     * @return $this
     */
    public function setStatus($Status) 
    {
        $this->Status = $Status;
        return $this;
    }

    /**
     * @return int
     */
    public function getParcelId() 
    {
        return $this->ParcelId;
    }

    /**
     * @param int $ParcelId
     * @return $this
     */
    public function setParcelId($ParcelId) 
    {
        $this->ParcelId = $ParcelId;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference() 
    {
        return $this->Reference;
    }

    /**
     * @param string $Reference
     * @return $this
     */
    public function setReference($Reference) 
    {
        $this->Reference = $Reference;
        return $this;
    }

    /**
     * @return string
     */
    public function getWaybill() 
    {
        return $this->Waybill;
    }

    /**
     * @param string $Waybill
     * @return $this
     */
    public function setWaybill($Waybill) 
    {
        $this->Waybill = $Waybill;
        return $this;
    }


    public function getValidationDetails()
    {
        return $this->ValidationDetails;
    }

    /**
     * @return $this
     */
    public function setValidationDetails($ValidationDetails) 
    {
        $this->ValidationDetails = $ValidationDetails;
        return $this;
    }


}

