<?php

namespace T3ko\Dpd\Soap\Types;

class DpdParcelBusinessEventDataV1
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $value;

    /**
     * @return string
     */
    public function getCode() 
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return $this
     */
    public function setCode($code) 
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getValue() 
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setValue($value) 
    {
        $this->value = $value;

        return $this;
    }
}
