<?php

namespace T3ko\Dpd\Soap\Types;

class DestinationDataList
{
    /**
     * @var DestinationsData
     */
    private $destinationsData;

    /**
     * @return DestinationsData
     */
    public function getDestinationsData() 
    {
        return $this->destinationsData;
    }

    /**
     * @param DestinationsData $destinationsData
     *
     * @return $this
     */
    public function setDestinationsData(DestinationsData $destinationsData) 
    {
        $this->destinationsData = $destinationsData;

        return $this;
    }
}
