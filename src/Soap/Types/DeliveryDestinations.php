<?php

namespace T3ko\Dpd\Soap\Types;

class DeliveryDestinations
{
    /**
     * @var DeliveryDestination
     */
    private $deliveryDestination;

    /**
     * @return DeliveryDestination
     */
    public function getDeliveryDestination() 
    {
        return $this->deliveryDestination;
    }

    /**
     * @param DeliveryDestination $deliveryDestination
     *
     * @return $this
     */
    public function setDeliveryDestination(DeliveryDestination $deliveryDestination) 
    {
        $this->deliveryDestination = $deliveryDestination;

        return $this;
    }
}
