<?php

namespace T3ko\Dpd\Soap\Types;

class ParcelPGRV1
{

    /**
     * @var int
     */
    private $parcelId;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var ValidationStatusPGREnumV1
     */
    private $status;

    /**
     * @var string
     */
    private $waybill;

    /**
     * @return int
     */
    public function getParcelId() 
    {
        return $this->parcelId;
    }

    /**
     * @param int $parcelId
     * @return $this
     */
    public function setParcelId($parcelId) 
    {
        $this->parcelId = $parcelId;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference() 
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return $this
     */
    public function setReference($reference) 
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return ValidationStatusPGREnumV1
     */
    public function getStatus() 
    {
        return $this->status;
    }

    /**
     * @param ValidationStatusPGREnumV1 $status
     * @return $this
     */
    public function setStatus(ValidationStatusPGREnumV1 $status) 
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getWaybill() 
    {
        return $this->waybill;
    }

    /**
     * @param string $waybill
     * @return $this
     */
    public function setWaybill($waybill) 
    {
        $this->waybill = $waybill;
        return $this;
    }


}

