<?php

namespace T3ko\Dpd\Soap\Types;

class ParcelDGRV2
{

    /**
     * @var int
     */
    private $parcelId;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var StatusInfoDGRV2
     */
    private $statusInfo;

    /**
     * @var string
     */
    private $waybill;

    /**
     * @return int
     */
    public function getParcelId() 
    {
        return $this->parcelId;
    }

    /**
     * @param int $parcelId
     * @return $this
     */
    public function setParcelId($parcelId) 
    {
        $this->parcelId = $parcelId;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference() 
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return $this
     */
    public function setReference($reference) 
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return StatusInfoDGRV2
     */
    public function getStatusInfo() 
    {
        return $this->statusInfo;
    }

    /**
     * @param StatusInfoDGRV2 $statusInfo
     * @return $this
     */
    public function setStatusInfo(StatusInfoDGRV2 $statusInfo) 
    {
        $this->statusInfo = $statusInfo;
        return $this;
    }

    /**
     * @return string
     */
    public function getWaybill() 
    {
        return $this->waybill;
    }

    /**
     * @param string $waybill
     * @return $this
     */
    public function setWaybill($waybill) 
    {
        $this->waybill = $waybill;
        return $this;
    }


}

