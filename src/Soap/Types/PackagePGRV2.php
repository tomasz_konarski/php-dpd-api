<?php

namespace T3ko\Dpd\Soap\Types;

class PackagePGRV2
{

    /**
     * @var string
     */
    private $Status;

    /**
     * @var int
     */
    private $PackageId;

    /**
     * @var string
     */
    private $Reference;

    /**
     * @var ValidationDetails
     */
    private $ValidationDetails;


    private $Parcels;

    /**
     * @return string
     */
    public function getStatus() 
    {
        return $this->Status;
    }

    /**
     * @param string $Status
     * @return $this
     */
    public function setStatus($Status) 
    {
        $this->Status = $Status;
        return $this;
    }

    /**
     * @return int
     */
    public function getPackageId() 
    {
        return $this->PackageId;
    }

    /**
     * @param int $PackageId
     * @return $this
     */
    public function setPackageId($PackageId) 
    {
        $this->PackageId = $PackageId;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference() 
    {
        return $this->Reference;
    }

    /**
     * @param string $Reference
     * @return $this
     */
    public function setReference($Reference) 
    {
        $this->Reference = $Reference;
        return $this;
    }

    public function getValidationDetails()
    {
        return $this->ValidationDetails;
    }

    /**
     * @return $this
     */
    public function setValidationDetails($ValidationDetails) 
    {
        $this->ValidationDetails = $ValidationDetails;
        return $this;
    }

    public function getParcels()
    {
        return $this->Parcels;
    }

    /**
     * @return $this
     */
    public function setParcels($Parcels) 
    {
        $this->Parcels = $Parcels;
        return $this;
    }


}

