<?php

namespace T3ko\Dpd\Soap\Types;

class PackageDSPV1
{

    /**
     * @var int
     */
    private $packageId;

    /**
     * @var ParcelDSPV1[]
     */
    private $parcels;

    /**
     * @var string
     */
    private $reference;

    /**
     * @return int
     */
    public function getPackageId() 
    {
        return $this->packageId;
    }

    /**
     * @param int $packageId
     * @return $this
     */
    public function setPackageId($packageId) 
    {
        $this->packageId = $packageId;
        return $this;
    }

    /**
     * @return ParcelDSPV1[]
     */
    public function getParcels() 
    {
        return $this->parcels;
    }

    /**
     * @param ParcelDSPV1[] $parcels
     * @return $this
     */
    public function setParcels(array $parcels) 
    {
        $this->parcels = $parcels;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference() 
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return $this
     */
    public function setReference($reference) 
    {
        $this->reference = $reference;
        return $this;
    }


}

