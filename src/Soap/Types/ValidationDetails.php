<?php

namespace T3ko\Dpd\Soap\Types;

class ValidationDetails
{

    /**
     * @var ValidationInfoPGRV2
     */
    private $validationInfo;

    /**
     * @return ValidationInfoPGRV2
     */
    public function getValidationInfo() 
    {
        return $this->validationInfo;
    }

    /**
     * @param ValidationInfoPGRV2 $validationInfo
     * @return $this
     */
    public function setValidationInfo(ValidationInfoPGRV2 $validationInfo) 
    {
        $this->validationInfo = $validationInfo;
        return $this;
    }


}

