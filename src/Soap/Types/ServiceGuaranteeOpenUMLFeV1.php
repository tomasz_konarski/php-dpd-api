<?php

namespace T3ko\Dpd\Soap\Types;

class ServiceGuaranteeOpenUMLFeV1
{

    /**
     * @var ServiceSelfColReceiverTypeEnumOpenUMLFeV1
     */
    private $type;

    /**
     * @var string
     */
    private $value;

    /**
     * @return ServiceSelfColReceiverTypeEnumOpenUMLFeV1
     */
    public function getType() 
    {
        return $this->type;
    }

    /**
     * @param ServiceSelfColReceiverTypeEnumOpenUMLFeV1 $type
     * @return $this
     */
    public function setType(ServiceSelfColReceiverTypeEnumOpenUMLFeV1 $type) 
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue() 
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue($value) 
    {
        $this->value = $value;
        return $this;
    }


}

