<?php

namespace T3ko\Dpd\Soap\Types;

class CourierOrderAvailabilityRangeV1
{
    /**
     * @var int
     */
    private $offset;

    /**
     * @var string
     */
    private $range;

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     *
     * @return $this
     */
    public function setOffset($offset) 
    {
        $this->offset = $offset;

        return $this;
    }

    /**
     * @return string
     */
    public function getRange()
    {
        return $this->range;
    }

    /**
     * @param string $range
     *
     * @return $this
     */
    public function setRange($range) 
    {
        $this->range = $range;

        return $this;
    }
}
