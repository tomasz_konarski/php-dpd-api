<?php

namespace T3ko\Dpd\Soap\Types;

class PostalCodeV1
{

    /**
     * @var string
     */
    private $countryCode;

    /**
     * @var string
     */
    private $zipCode;

    /**
     * @return string
     */
    public function getCountryCode() 
    {
        return $this->countryCode;
    }

    /**
     * @param string $countryCode
     * @return $this
     */
    public function setCountryCode($countryCode) 
    {
        $this->countryCode = $countryCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getZipCode() 
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     * @return $this
     */
    public function setZipCode($zipCode) 
    {
        $this->zipCode = $zipCode;
        return $this;
    }


}

