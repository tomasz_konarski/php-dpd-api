<?php

namespace T3ko\Dpd\Soap\Types;

class PickupPackagesParamsDPPV1
{

    /**
     * @var bool
     */
    private $dox;

    /**
     * @var int
     */
    private $doxCount;

    /**
     * @var bool
     */
    private $pallet;

    /**
     * @var float
     */
    private $palletMaxHeight;

    /**
     * @var float
     */
    private $palletMaxWeight;

    /**
     * @var int
     */
    private $palletsCount;

    /**
     * @var float
     */
    private $palletsWeight;

    /**
     * @var float
     */
    private $parcelMaxDepth;

    /**
     * @var float
     */
    private $parcelMaxHeight;

    /**
     * @var float
     */
    private $parcelMaxWeight;

    /**
     * @var float
     */
    private $parcelMaxWidth;

    /**
     * @var int
     */
    private $parcelsCount;

    /**
     * @var float
     */
    private $parcelsWeight;

    /**
     * @var bool
     */
    private $standardParcel;

    /**
     * @return bool
     */
    public function isDox() 
    {
        return $this->dox;
    }

    /**
     * @param bool $dox
     * @return $this
     */
    public function setDox($dox) 
    {
        $this->dox = $dox;
        return $this;
    }

    /**
     * @return int
     */
    public function getDoxCount() 
    {
        return $this->doxCount;
    }

    /**
     * @param int $doxCount
     * @return $this
     */
    public function setDoxCount($doxCount) 
    {
        $this->doxCount = $doxCount;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPallet() 
    {
        return $this->pallet;
    }

    /**
     * @param bool $pallet
     * @return $this
     */
    public function setPallet($pallet) 
    {
        $this->pallet = $pallet;
        return $this;
    }

    /**
     * @return float
     */
    public function getPalletMaxHeight() 
    {
        return $this->palletMaxHeight;
    }

    /**
     * @param float $palletMaxHeight
     * @return $this
     */
    public function setPalletMaxHeight(float $palletMaxHeight) 
    {
        $this->palletMaxHeight = $palletMaxHeight;
        return $this;
    }

    /**
     * @return float
     */
    public function getPalletMaxWeight() 
    {
        return $this->palletMaxWeight;
    }

    /**
     * @param float $palletMaxWeight
     * @return $this
     */
    public function setPalletMaxWeight(float $palletMaxWeight) 
    {
        $this->palletMaxWeight = $palletMaxWeight;
        return $this;
    }

    /**
     * @return int
     */
    public function getPalletsCount() 
    {
        return $this->palletsCount;
    }

    /**
     * @param int $palletsCount
     * @return $this
     */
    public function setPalletsCount($palletsCount) 
    {
        $this->palletsCount = $palletsCount;
        return $this;
    }

    /**
     * @return float
     */
    public function getPalletsWeight() 
    {
        return $this->palletsWeight;
    }

    /**
     * @param float $palletsWeight
     * @return $this
     */
    public function setPalletsWeight(float $palletsWeight) 
    {
        $this->palletsWeight = $palletsWeight;
        return $this;
    }

    /**
     * @return float
     */
    public function getParcelMaxDepth() 
    {
        return $this->parcelMaxDepth;
    }

    /**
     * @param float $parcelMaxDepth
     * @return $this
     */
    public function setParcelMaxDepth(float $parcelMaxDepth) 
    {
        $this->parcelMaxDepth = $parcelMaxDepth;
        return $this;
    }

    /**
     * @return float
     */
    public function getParcelMaxHeight() 
    {
        return $this->parcelMaxHeight;
    }

    /**
     * @param float $parcelMaxHeight
     * @return $this
     */
    public function setParcelMaxHeight(float $parcelMaxHeight) 
    {
        $this->parcelMaxHeight = $parcelMaxHeight;
        return $this;
    }

    /**
     * @return float
     */
    public function getParcelMaxWeight() 
    {
        return $this->parcelMaxWeight;
    }

    /**
     * @param float $parcelMaxWeight
     * @return $this
     */
    public function setParcelMaxWeight(float $parcelMaxWeight) 
    {
        $this->parcelMaxWeight = $parcelMaxWeight;
        return $this;
    }

    /**
     * @return float
     */
    public function getParcelMaxWidth() 
    {
        return $this->parcelMaxWidth;
    }

    /**
     * @param float $parcelMaxWidth
     * @return $this
     */
    public function setParcelMaxWidth(float $parcelMaxWidth) 
    {
        $this->parcelMaxWidth = $parcelMaxWidth;
        return $this;
    }

    /**
     * @return int
     */
    public function getParcelsCount() 
    {
        return $this->parcelsCount;
    }

    /**
     * @param int $parcelsCount
     * @return $this
     */
    public function setParcelsCount($parcelsCount) 
    {
        $this->parcelsCount = $parcelsCount;
        return $this;
    }

    /**
     * @return float
     */
    public function getParcelsWeight() 
    {
        return $this->parcelsWeight;
    }

    /**
     * @param float $parcelsWeight
     * @return $this
     */
    public function setParcelsWeight(float $parcelsWeight) 
    {
        $this->parcelsWeight = $parcelsWeight;
        return $this;
    }

    /**
     * @return bool
     */
    public function isStandardParcel() 
    {
        return $this->standardParcel;
    }

    /**
     * @param bool $standardParcel
     * @return $this
     */
    public function setStandardParcel($standardParcel) 
    {
        $this->standardParcel = $standardParcel;
        return $this;
    }


}

