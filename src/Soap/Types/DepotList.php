<?php

namespace T3ko\Dpd\Soap\Types;

class DepotList
{
    /**
     * @var ProtocolDepot
     */
    private $protocolDepot;

    /**
     * @return ProtocolDepot
     */
    public function getProtocolDepot() 
    {
        return $this->protocolDepot;
    }

    /**
     * @param ProtocolDepot $protocolDepot
     *
     * @return $this
     */
    public function setProtocolDepot(ProtocolDepot $protocolDepot) 
    {
        $this->protocolDepot = $protocolDepot;

        return $this;
    }
}
