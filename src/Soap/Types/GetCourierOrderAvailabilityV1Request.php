<?php

namespace T3ko\Dpd\Soap\Types;

use Phpro\SoapClient\Type\RequestInterface;

class GetCourierOrderAvailabilityV1Request implements RequestInterface
{

    /**
     * @var SenderPlaceV1
     */
    private $senderPlaceV1;

    /**
     * @var AuthDataV1
     */
    private $authDataV1;

    /**
     * @return SenderPlaceV1
     */
    public function getSenderPlace() 
    {
        return $this->senderPlaceV1;
    }

    /**
     * @param SenderPlaceV1 $senderPlaceV1
     * @return $this
     */
    public function setSenderPlace(SenderPlaceV1 $senderPlaceV1) 
    {
        $this->senderPlaceV1 = $senderPlaceV1;
        return $this;
    }

    /**
     * @return AuthDataV1
     */
    public function getAuthData() 
    {
        return $this->authDataV1;
    }

    /**
     * @param AuthDataV1 $authDataV1
     * @return $this
     */
    public function setAuthData(AuthDataV1 $authDataV1) 
    {
        $this->authDataV1 = $authDataV1;
        return $this;
    }


}

