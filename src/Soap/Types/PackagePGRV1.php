<?php

namespace T3ko\Dpd\Soap\Types;

class PackagePGRV1
{

    /**
     * @var InvalidFieldPGRV1
     */
    private $invalidFields;

    /**
     * @var int
     */
    private $packageId;

    /**
     * @var ParcelPGRV1
     */
    private $parcels;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var ValidationStatusPGREnumV1
     */
    private $status;

    /**
     * @return InvalidFieldPGRV1
     */
    public function getInvalidFields() 
    {
        return $this->invalidFields;
    }

    /**
     * @param InvalidFieldPGRV1 $invalidFields
     * @return $this
     */
    public function setInvalidFields(InvalidFieldPGRV1 $invalidFields) 
    {
        $this->invalidFields = $invalidFields;
        return $this;
    }

    /**
     * @return int
     */
    public function getPackageId() 
    {
        return $this->packageId;
    }

    /**
     * @param int $packageId
     * @return $this
     */
    public function setPackageId($packageId) 
    {
        $this->packageId = $packageId;
        return $this;
    }

    /**
     * @return ParcelPGRV1
     */
    public function getParcels() 
    {
        return $this->parcels;
    }

    /**
     * @param ParcelPGRV1 $parcels
     * @return $this
     */
    public function setParcels(ParcelPGRV1 $parcels) 
    {
        $this->parcels = $parcels;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference() 
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return $this
     */
    public function setReference($reference) 
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return ValidationStatusPGREnumV1
     */
    public function getStatus() 
    {
        return $this->status;
    }

    /**
     * @param ValidationStatusPGREnumV1 $status
     * @return $this
     */
    public function setStatus(ValidationStatusPGREnumV1 $status) 
    {
        $this->status = $status;
        return $this;
    }


}

