<?php

namespace T3ko\Dpd\Soap\Types;

class PackagesPickupCallResponseV1
{

    /**
     * @var string
     */
    private $orderNumber;

    /**
     * @var ProtocolPCRV1
     */
    private $prototocols;

    /**
     * @return string
     */
    public function getOrderNumber() 
    {
        return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     * @return $this
     */
    public function setOrderNumber($orderNumber) 
    {
        $this->orderNumber = $orderNumber;
        return $this;
    }

    /**
     * @return ProtocolPCRV1
     */
    public function getPrototocols() 
    {
        return $this->prototocols;
    }

    /**
     * @param ProtocolPCRV1 $prototocols
     * @return $this
     */
    public function setPrototocols(ProtocolPCRV1 $prototocols) 
    {
        $this->prototocols = $prototocols;
        return $this;
    }


}

