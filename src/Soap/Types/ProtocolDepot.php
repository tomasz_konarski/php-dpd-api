<?php

namespace T3ko\Dpd\Soap\Types;

class ProtocolDepot
{

    /**
     * @var string
     */
    private $number;

    /**
     * @return string
     */
    public function getNumber() 
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return $this
     */
    public function setNumber($number) 
    {
        $this->number = $number;
        return $this;
    }


}

