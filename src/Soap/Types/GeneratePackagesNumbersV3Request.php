<?php

namespace T3ko\Dpd\Soap\Types;

use Phpro\SoapClient\Type\RequestInterface;

class GeneratePackagesNumbersV3Request implements RequestInterface
{
    /**
     * @var OpenUMLFeV2
     */
    private $openUMLFeV2;

    /**
     * @var PkgNumsGenerationPolicyV1
     */
    private $pkgNumsGenerationPolicyV1;

    /**
     * @var string
     */
    private $langCode;

    /**
     * @var AuthDataV1
     */
    private $authDataV1;

    /**
     * @return OpenUMLFeV2
     */
    public function getOpenUMLFe() 
    {
        return $this->openUMLFeV2;
    }

    /**
     * @param OpenUMLFeV2 $openUMLFeV2
     *
     * @return $this
     */
    public function setOpenUMLFe(OpenUMLFeV2 $openUMLFeV2) 
    {
        $this->openUMLFeV2 = $openUMLFeV2;

        return $this;
    }

    /**
     * @return PkgNumsGenerationPolicyV1
     */
    public function getPkgNumsGenerationPolicy() 
    {
        return $this->pkgNumsGenerationPolicyV1;
    }

    /**
     * @param PkgNumsGenerationPolicyV1 $pkgNumsGenerationPolicyV1
     *
     * @return $this
     */
    public function setPkgNumsGenerationPolicy(PkgNumsGenerationPolicyV1 $pkgNumsGenerationPolicyV1) 
    {
        $this->pkgNumsGenerationPolicyV1 = $pkgNumsGenerationPolicyV1;

        return $this;
    }

    /**
     * @return string
     */
    public function getLangCode() 
    {
        return $this->langCode;
    }

    /**
     * @param string $langCode
     *
     * @return $this
     */
    public function setLangCode($langCode) 
    {
        $this->langCode = $langCode;

        return $this;
    }

    /**
     * @return AuthDataV1
     */
    public function getAuthData() 
    {
        return $this->authDataV1;
    }

    /**
     * @param AuthDataV1 $authDataV1
     *
     * @return $this
     */
    public function setAuthData(AuthDataV1 $authDataV1) 
    {
        $this->authDataV1 = $authDataV1;

        return $this;
    }
}
