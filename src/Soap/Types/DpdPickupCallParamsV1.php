<?php

namespace T3ko\Dpd\Soap\Types;

class DpdPickupCallParamsV1
{
    /**
     * @var ContactInfoDPPV1
     */
    private $contactInfo;

    /**
     * @var PickupAddressDSPV1
     */
    private $pickupAddress;

    /**
     * @var \DateTime
     */
    private $pickupDate;

    /**
     * @var string
     */
    private $pickupTimeFrom;

    /**
     * @var string
     */
    private $pickupTimeTo;

    /**
     * @var PolicyDPPEnumV1
     */
    private $policy;

    /**
     * @var ProtocolDPPV1
     */
    private $protocols;

    /**
     * @return ContactInfoDPPV1
     */
    public function getContactInfo() 
    {
        return $this->contactInfo;
    }

    /**
     * @param ContactInfoDPPV1 $contactInfo
     *
     * @return $this
     */
    public function setContactInfo(ContactInfoDPPV1 $contactInfo) 
    {
        $this->contactInfo = $contactInfo;

        return $this;
    }

    /**
     * @return PickupAddressDSPV1
     */
    public function getPickupAddress() 
    {
        return $this->pickupAddress;
    }

    /**
     * @param PickupAddressDSPV1 $pickupAddress
     *
     * @return $this
     */
    public function setPickupAddress(PickupAddressDSPV1 $pickupAddress) 
    {
        $this->pickupAddress = $pickupAddress;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPickupDate() : \DateTime
    {
        return $this->pickupDate;
    }

    /**
     * @param \DateTime $pickupDate
     *
     * @return $this
     */
    public function setPickupDate($pickupDate) 
    {
        $this->pickupDate = $pickupDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getPickupTimeFrom() 
    {
        return $this->pickupTimeFrom;
    }

    /**
     * @param string $pickupTimeFrom
     *
     * @return $this
     */
    public function setPickupTimeFrom($pickupTimeFrom) 
    {
        $this->pickupTimeFrom = $pickupTimeFrom;

        return $this;
    }

    /**
     * @return string
     */
    public function getPickupTimeTo() 
    {
        return $this->pickupTimeTo;
    }

    /**
     * @param string $pickupTimeTo
     *
     * @return $this
     */
    public function setPickupTimeTo($pickupTimeTo) 
    {
        $this->pickupTimeTo = $pickupTimeTo;

        return $this;
    }

    /**
     * @return PolicyDPPEnumV1
     */
    public function getPolicy() 
    {
        return $this->policy;
    }

    /**
     * @param PolicyDPPEnumV1 $policy
     *
     * @return $this
     */
    public function setPolicy(PolicyDPPEnumV1 $policy) 
    {
        $this->policy = $policy;

        return $this;
    }

    /**
     * @return ProtocolDPPV1
     */
    public function getProtocols() 
    {
        return $this->protocols;
    }

    /**
     * @param ProtocolDPPV1 $protocols
     *
     * @return $this
     */
    public function setProtocols(ProtocolDPPV1 $protocols) 
    {
        $this->protocols = $protocols;

        return $this;
    }
}
