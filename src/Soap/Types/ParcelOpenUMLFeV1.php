<?php

namespace T3ko\Dpd\Soap\Types;

class ParcelOpenUMLFeV1
{

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $customerData1;

    /**
     * @var string
     */
    private $customerData2;

    /**
     * @var string
     */
    private $customerData3;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var int
     */
    private $sizeX;

    /**
     * @var int
     */
    private $sizeY;

    /**
     * @var int
     */
    private $sizeZ;

    /**
     * @var float
     */
    private $weight;

    /**
     * @return string
     */
    public function getContent() 
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return $this
     */
    public function setContent($content) 
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerData1() 
    {
        return $this->customerData1;
    }

    /**
     * @param string $customerData1
     * @return $this
     */
    public function setCustomerData1($customerData1) 
    {
        $this->customerData1 = $customerData1;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerData2() 
    {
        return $this->customerData2;
    }

    /**
     * @param string $customerData2
     * @return $this
     */
    public function setCustomerData2($customerData2) 
    {
        $this->customerData2 = $customerData2;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerData3() 
    {
        return $this->customerData3;
    }

    /**
     * @param string $customerData3
     * @return $this
     */
    public function setCustomerData3($customerData3) 
    {
        $this->customerData3 = $customerData3;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference() 
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return $this
     */
    public function setReference($reference) 
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return int
     */
    public function getSizeX() 
    {
        return $this->sizeX;
    }

    /**
     * @param int $sizeX
     * @return $this
     */
    public function setSizeX($sizeX) 
    {
        $this->sizeX = $sizeX;
        return $this;
    }

    /**
     * @return int
     */
    public function getSizeY() 
    {
        return $this->sizeY;
    }

    /**
     * @param int $sizeY
     * @return $this
     */
    public function setSizeY($sizeY) 
    {
        $this->sizeY = $sizeY;
        return $this;
    }

    /**
     * @return int
     */
    public function getSizeZ() 
    {
        return $this->sizeZ;
    }

    /**
     * @param int $sizeZ
     * @return $this
     */
    public function setSizeZ($sizeZ) 
    {
        $this->sizeZ = $sizeZ;
        return $this;
    }

    /**
     * @return float
     */
    public function getWeight() 
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     * @return $this
     */
    public function setWeight(float $weight) 
    {
        $this->weight = $weight;
        return $this;
    }


}

