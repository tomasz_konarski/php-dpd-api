<?php

namespace T3ko\Dpd\Soap\Types;

class ErrorDetailsPCRV2
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $fields;

    /**
     * @return string
     */
    public function getCode() 
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return $this
     */
    public function setCode($code) 
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription() 
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description) 
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getFields() 
    {
        return $this->fields;
    }

    /**
     * @param string $fields
     *
     * @return $this
     */
    public function setFields($fields) 
    {
        $this->fields = $fields;

        return $this;
    }
}
