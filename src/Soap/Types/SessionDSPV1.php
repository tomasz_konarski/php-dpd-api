<?php

namespace T3ko\Dpd\Soap\Types;

class SessionDSPV1
{

    /**
     * @var PackageDSPV1[]
     */
    private $packages;

    /**
     * @var int
     */
    private $sessionId;

    /**
     * @var SessionTypeDSPEnumV1
     */
    private $sessionType;

    /**
     * @return PackageDSPV1[]
     */
    public function getPackages() 
    {
        return $this->packages;
    }

    /**
     * @param array $packages
     * @return $this
     */
    public function setPackages(array $packages) 
    {
        $this->packages = $packages;
        return $this;
    }

    /**
     * @return int
     */
    public function getSessionId() 
    {
        return $this->sessionId;
    }

    /**
     * @param int $sessionId
     * @return $this
     */
    public function setSessionId($sessionId) 
    {
        $this->sessionId = $sessionId;
        return $this;
    }

    /**
     * @return SessionTypeDSPEnumV1
     */
    public function getSessionType() 
    {
        return $this->sessionType;
    }

    /**
     * @param SessionTypeDSPEnumV1 $sessionType
     * @return $this
     */
    public function setSessionType(SessionTypeDSPEnumV1 $sessionType) 
    {
        $this->sessionType = $sessionType;
        return $this;
    }


}

