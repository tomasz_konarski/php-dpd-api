<?php

namespace T3ko\Dpd\Soap\Types;

class PackageOpenUMLFeV3
{

    /**
     * @var ParcelOpenUMLFeV1
     */
    private $parcels;

    /**
     * @var PayerTypeEnumOpenUMLFeV1
     */
    private $payerType;

    /**
     * @var PackageAddressOpenUMLFeV1
     */
    private $receiver;

    /**
     * @var string
     */
    private $ref1;

    /**
     * @var string
     */
    private $ref2;

    /**
     * @var string
     */
    private $ref3;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var PackageAddressOpenUMLFeV1
     */
    private $sender;

    /**
     * @var ServicesOpenUMLFeV4
     */
    private $services;

    /**
     * @var int
     */
    private $thirdPartyFID;

    /**
     * @return ParcelOpenUMLFeV1
     */
    public function getParcels() 
    {
        return $this->parcels;
    }

    /**
     * @param ParcelOpenUMLFeV1[] $parcels
     * @return $this
     */
    public function setParcels(array $parcels) 
    {
        $this->parcels = $parcels;
        return $this;
    }

    /**
     * @return PayerTypeEnumOpenUMLFeV1
     */
    public function getPayerType() 
    {
        return $this->payerType;
    }

    /**
     * @param PayerTypeEnumOpenUMLFeV1 $payerType
     * @return $this
     */
    public function setPayerType(PayerTypeEnumOpenUMLFeV1 $payerType) 
    {
        $this->payerType = $payerType;
        return $this;
    }

    /**
     * @return PackageAddressOpenUMLFeV1
     */
    public function getReceiver() 
    {
        return $this->receiver;
    }

    /**
     * @param PackageAddressOpenUMLFeV1 $receiver
     * @return $this
     */
    public function setReceiver(PackageAddressOpenUMLFeV1 $receiver) 
    {
        $this->receiver = $receiver;
        return $this;
    }

    /**
     * @return string
     */
    public function getRef1() 
    {
        return $this->ref1;
    }

    /**
     * @param string $ref1
     * @return $this
     */
    public function setRef1($ref1) 
    {
        $this->ref1 = $ref1;
        return $this;
    }

    /**
     * @return string
     */
    public function getRef2() 
    {
        return $this->ref2;
    }

    /**
     * @param string $ref2
     * @return $this
     */
    public function setRef2($ref2) 
    {
        $this->ref2 = $ref2;
        return $this;
    }

    /**
     * @return string
     */
    public function getRef3() 
    {
        return $this->ref3;
    }

    /**
     * @param string $ref3
     * @return $this
     */
    public function setRef3($ref3) 
    {
        $this->ref3 = $ref3;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference() 
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return $this
     */
    public function setReference($reference) 
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return PackageAddressOpenUMLFeV1
     */
    public function getSender() 
    {
        return $this->sender;
    }

    /**
     * @param PackageAddressOpenUMLFeV1 $sender
     * @return $this
     */
    public function setSender(PackageAddressOpenUMLFeV1 $sender) 
    {
        $this->sender = $sender;
        return $this;
    }

    /**
     * @return ServicesOpenUMLFeV4
     */
    public function getServices() 
    {
        return $this->services;
    }

    /**
     * @param ServicesOpenUMLFeV4 $services
     * @return $this
     */
    public function setServices(ServicesOpenUMLFeV4 $services) 
    {
        $this->services = $services;
        return $this;
    }

    /**
     * @return int
     */
    public function getThirdPartyFID() 
    {
        return $this->thirdPartyFID;
    }

    /**
     * @param int $thirdPartyFID
     * @return $this
     */
    public function setThirdPartyFID($thirdPartyFID) 
    {
        $this->thirdPartyFID = $thirdPartyFID;
        return $this;
    }


}

