<?php

namespace T3ko\Dpd\Soap\Types;

class PackageDGRV1
{

    /**
     * @var int
     */
    private $packageId;

    /**
     * @var ParcelDGRV1
     */
    private $parcels;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var StatusInfoDGRV1
     */
    private $statusInfo;

    /**
     * @return int
     */
    public function getPackageId() 
    {
        return $this->packageId;
    }

    /**
     * @param int $packageId
     * @return $this
     */
    public function setPackageId($packageId) 
    {
        $this->packageId = $packageId;
        return $this;
    }

    /**
     * @return ParcelDGRV1
     */
    public function getParcels() 
    {
        return $this->parcels;
    }

    /**
     * @param ParcelDGRV1 $parcels
     * @return $this
     */
    public function setParcels(ParcelDGRV1 $parcels) 
    {
        $this->parcels = $parcels;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference() 
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return $this
     */
    public function setReference($reference) 
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return StatusInfoDGRV1
     */
    public function getStatusInfo() 
    {
        return $this->statusInfo;
    }

    /**
     * @param StatusInfoDGRV1 $statusInfo
     * @return $this
     */
    public function setStatusInfo(StatusInfoDGRV1 $statusInfo) 
    {
        $this->statusInfo = $statusInfo;
        return $this;
    }


}

