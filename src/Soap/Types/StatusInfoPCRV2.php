<?php

namespace T3ko\Dpd\Soap\Types;

class StatusInfoPCRV2
{

    /**
     * @var ErrorDetailsPCRV2
     */
    private $errorDetails;

    /**
     * @var string
     */
    private $status;

    /**
     * @return ErrorDetailsPCRV2
     */
    public function getErrorDetails() 
    {
        return $this->errorDetails;
    }

    /**
     * @param ErrorDetailsPCRV2 $errorDetails
     * @return $this
     */
    public function setErrorDetails(ErrorDetailsPCRV2 $errorDetails) 
    {
        $this->errorDetails = $errorDetails;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus() 
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status) 
    {
        $this->status = $status;
        return $this;
    }


}

