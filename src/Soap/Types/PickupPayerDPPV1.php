<?php

namespace T3ko\Dpd\Soap\Types;

class PickupPayerDPPV1
{

    /**
     * @var string
     */
    private $payerCostCenter;

    /**
     * @var string
     */
    private $payerName;

    /**
     * @var int
     */
    private $payerNumber;

    /**
     * @return string
     */
    public function getPayerCostCenter() 
    {
        return $this->payerCostCenter;
    }

    /**
     * @param string $payerCostCenter
     * @return $this
     */
    public function setPayerCostCenter($payerCostCenter) 
    {
        $this->payerCostCenter = $payerCostCenter;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayerName() 
    {
        return $this->payerName;
    }

    /**
     * @param string $payerName
     * @return $this
     */
    public function setPayerName($payerName) 
    {
        $this->payerName = $payerName;
        return $this;
    }

    /**
     * @return int
     */
    public function getPayerNumber() 
    {
        return $this->payerNumber;
    }

    /**
     * @param int $payerNumber
     * @return $this
     */
    public function setPayerNumber($payerNumber) 
    {
        $this->payerNumber = $payerNumber;
        return $this;
    }


}

