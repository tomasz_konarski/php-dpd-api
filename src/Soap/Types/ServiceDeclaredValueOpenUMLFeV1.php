<?php

namespace T3ko\Dpd\Soap\Types;

class ServiceDeclaredValueOpenUMLFeV1
{

    /**
     * @var string
     */
    private $amount;

    /**
     * @var ServiceCurrencyEnum
     */
    private $currency;

    /**
     * @return string
     */
    public function getAmount() 
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     * @return $this
     */
    public function setAmount($amount) 
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return ServiceCurrencyEnum
     */
    public function getCurrency() 
    {
        return $this->currency;
    }

    /**
     * @param ServiceCurrencyEnum $currency
     * @return $this
     */
    public function setCurrency(ServiceCurrencyEnum $currency) 
    {
        $this->currency = $currency;
        return $this;
    }


}

