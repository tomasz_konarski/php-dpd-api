<?php

namespace T3ko\Dpd\Soap\Types;

class ParcelsAppendV1
{

    /**
     * @var ParcelsAppendSearchCriteriaPAV1
     */
    private $packagesearchCriteria;

    /**
     * @var ParcelAppendPAV1
     */
    private $parcels;

    /**
     * @return ParcelsAppendSearchCriteriaPAV1
     */
    public function getPackagesearchCriteria() 
    {
        return $this->packagesearchCriteria;
    }

    /**
     * @param ParcelsAppendSearchCriteriaPAV1 $packagesearchCriteria
     * @return $this
     */
    public function setPackagesearchCriteria(ParcelsAppendSearchCriteriaPAV1 $packagesearchCriteria) 
    {
        $this->packagesearchCriteria = $packagesearchCriteria;
        return $this;
    }

    /**
     * @return ParcelAppendPAV1
     */
    public function getParcels() 
    {
        return $this->parcels;
    }

    /**
     * @param ParcelAppendPAV1 $parcels
     * @return $this
     */
    public function setParcels(ParcelAppendPAV1 $parcels) 
    {
        $this->parcels = $parcels;
        return $this;
    }


}

