<?php

namespace T3ko\Dpd\Soap\Types;

class ContactInfoDPPV1
{
    /**
     * @var string
     */
    private $comments;

    /**
     * @var string
     */
    private $company;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $phone;

    /**
     * @return string
     */
    public function getComments() 
    {
        return $this->comments;
    }

    /**
     * @param string $comments
     *
     * @return $this
     */
    public function setComments($comments) 
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompany() 
    {
        return $this->company;
    }

    /**
     * @param string $company
     *
     * @return $this
     */
    public function setCompany($company) 
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail() 
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email) 
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getName() 
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name) 
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone() 
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return $this
     */
    public function setPhone($phone) 
    {
        $this->phone = $phone;

        return $this;
    }
}
