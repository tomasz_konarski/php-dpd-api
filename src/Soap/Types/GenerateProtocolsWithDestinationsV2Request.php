<?php

namespace T3ko\Dpd\Soap\Types;

use Phpro\SoapClient\Type\RequestInterface;

class GenerateProtocolsWithDestinationsV2Request implements RequestInterface
{
    /**
     * @var DpdServicesParamsV2
     */
    private $dpdServicesParamsV2;

    /**
     * @var AuthDataV1
     */
    private $authDataV1;

    /**
     * @return DpdServicesParamsV2
     */
    public function getDpdServicesParams() 
    {
        return $this->dpdServicesParamsV2;
    }

    /**
     * @param DpdServicesParamsV2 $dpdServicesParamsV2
     *
     * @return $this
     */
    public function setDpdServicesParams(DpdServicesParamsV2 $dpdServicesParamsV2) 
    {
        $this->dpdServicesParamsV2 = $dpdServicesParamsV2;

        return $this;
    }

    /**
     * @return AuthDataV1
     */
    public function getAuthData() 
    {
        return $this->authDataV1;
    }

    /**
     * @param AuthDataV1 $authDataV1
     *
     * @return $this
     */
    public function setAuthData(AuthDataV1 $authDataV1) 
    {
        $this->authDataV1 = $authDataV1;

        return $this;
    }
}
