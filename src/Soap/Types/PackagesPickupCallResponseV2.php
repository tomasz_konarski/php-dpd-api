<?php

namespace T3ko\Dpd\Soap\Types;

class PackagesPickupCallResponseV2
{

    /**
     * @var string
     */
    private $orderNumber;

    /**
     * @var StatusInfoPCRV2
     */
    private $statusInfo;

    /**
     * @return string
     */
    public function getOrderNumber() 
    {
        return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     * @return $this
     */
    public function setOrderNumber($orderNumber) 
    {
        $this->orderNumber = $orderNumber;
        return $this;
    }

    /**
     * @return StatusInfoPCRV2
     */
    public function getStatusInfo() 
    {
        return $this->statusInfo;
    }

    /**
     * @param StatusInfoPCRV2 $statusInfo
     * @return $this
     */
    public function setStatusInfo(StatusInfoPCRV2 $statusInfo) 
    {
        $this->statusInfo = $statusInfo;
        return $this;
    }


}

