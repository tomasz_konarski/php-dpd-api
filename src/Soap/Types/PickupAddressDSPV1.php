<?php

namespace T3ko\Dpd\Soap\Types;

class PickupAddressDSPV1
{

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $company;

    /**
     * @var string
     */
    private $countryCode;

    /**
     * @var string
     */
    private $email;

    /**
     * @var int
     */
    private $fid;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $postalCode;

    /**
     * @return string
     */
    public function getAddress() 
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return $this
     */
    public function setAddress($address) 
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity() 
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return $this
     */
    public function setCity($city) 
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getCompany() 
    {
        return $this->company;
    }

    /**
     * @param string $company
     * @return $this
     */
    public function setCompany($company) 
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountryCode() 
    {
        return $this->countryCode;
    }

    /**
     * @param string $countryCode
     * @return $this
     */
    public function setCountryCode($countryCode) 
    {
        $this->countryCode = $countryCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail() 
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email) 
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return int
     */
    public function getFid() 
    {
        return $this->fid;
    }

    /**
     * @param int $fid
     * @return $this
     */
    public function setFid($fid) 
    {
        $this->fid = $fid;
        return $this;
    }

    /**
     * @return string
     */
    public function getName() 
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name) 
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone() 
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return $this
     */
    public function setPhone($phone) 
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostalCode() 
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     * @return $this
     */
    public function setPostalCode($postalCode) 
    {
        $this->postalCode = $postalCode;
        return $this;
    }


}

