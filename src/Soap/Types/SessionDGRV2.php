<?php

namespace T3ko\Dpd\Soap\Types;

class SessionDGRV2
{

    /**
     * @var Packages
     */
    private $packages;

    /**
     * @var int
     */
    private $sessionId;

    /**
     * @var StatusInfoDGRV2
     */
    private $statusInfo;

    /**
     * @return Packages
     */
    public function getPackages() 
    {
        return $this->packages;
    }

    /**
     * @param Packages $packages
     * @return $this
     */
    public function setPackages(Packages $packages) 
    {
        $this->packages = $packages;
        return $this;
    }

    /**
     * @return int
     */
    public function getSessionId() 
    {
        return $this->sessionId;
    }

    /**
     * @param int $sessionId
     * @return $this
     */
    public function setSessionId($sessionId) 
    {
        $this->sessionId = $sessionId;
        return $this;
    }

    /**
     * @return StatusInfoDGRV2
     */
    public function getStatusInfo() 
    {
        return $this->statusInfo;
    }

    /**
     * @param StatusInfoDGRV2 $statusInfo
     * @return $this
     */
    public function setStatusInfo(StatusInfoDGRV2 $statusInfo) 
    {
        $this->statusInfo = $statusInfo;
        return $this;
    }


}

