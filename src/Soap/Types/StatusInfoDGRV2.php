<?php

namespace T3ko\Dpd\Soap\Types;

class StatusInfoDGRV2
{

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $status;

    /**
     * @return string
     */
    public function getDescription() 
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description) 
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus() 
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status) 
    {
        $this->status = $status;
        return $this;
    }


}

