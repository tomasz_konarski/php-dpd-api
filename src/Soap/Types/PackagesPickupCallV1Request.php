<?php

namespace T3ko\Dpd\Soap\Types;

use Phpro\SoapClient\Type\RequestInterface;

class PackagesPickupCallV1Request implements RequestInterface
{

    /**
     * @var DpdPickupCallParamsV1
     */
    private $dpdPickupParamsV1;

    /**
     * @var AuthDataV1
     */
    private $authDataV1;

    /**
     * @return DpdPickupCallParamsV1
     */
    public function getDpdPickupParams() 
    {
        return $this->dpdPickupParamsV1;
    }

    /**
     * @param DpdPickupCallParamsV1 $dpdPickupParamsV1
     * @return $this
     */
    public function setDpdPickupParams(DpdPickupCallParamsV1 $dpdPickupParamsV1) 
    {
        $this->dpdPickupParamsV1 = $dpdPickupParamsV1;
        return $this;
    }

    /**
     * @return AuthDataV1
     */
    public function getAuthData() 
    {
        return $this->authDataV1;
    }

    /**
     * @param AuthDataV1 $authDataV1
     * @return $this
     */
    public function setAuthData(AuthDataV1 $authDataV1) 
    {
        $this->authDataV1 = $authDataV1;
        return $this;
    }


}

