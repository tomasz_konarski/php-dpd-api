<?php

namespace T3ko\Dpd\Soap\Types;

class DeniedAccessWSException
{
    /**
     * @var int
     */
    private $errorCode;

    /**
     * @var string
     */
    private $exceptionDetails;

    /**
     * @var string
     */
    private $message;

    /**
     * @return int
     */
    public function getErrorCode() 
    {
        return $this->errorCode;
    }

    /**
     * @param int $errorCode
     *
     * @return $this
     */
    public function setErrorCode($errorCode) 
    {
        $this->errorCode = $errorCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getExceptionDetails() 
    {
        return $this->exceptionDetails;
    }

    /**
     * @param string $exceptionDetails
     *
     * @return $this
     */
    public function setExceptionDetails($exceptionDetails) 
    {
        $this->exceptionDetails = $exceptionDetails;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage() 
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return $this
     */
    public function setMessage($message) 
    {
        $this->message = $message;

        return $this;
    }
}
