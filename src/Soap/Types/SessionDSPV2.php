<?php

namespace T3ko\Dpd\Soap\Types;

class SessionDSPV2
{

    /**
     * @var Packages
     */
    private $packages;

    /**
     * @var int
     */
    private $sessionId;

    /**
     * @return Packages
     */
    public function getPackages() 
    {
        return $this->packages;
    }

    /**
     * @param Packages $packages
     * @return $this
     */
    public function setPackages(Packages $packages) 
    {
        $this->packages = $packages;
        return $this;
    }

    /**
     * @return int
     */
    public function getSessionId() 
    {
        return $this->sessionId;
    }

    /**
     * @param int $sessionId
     * @return $this
     */
    public function setSessionId($sessionId) 
    {
        $this->sessionId = $sessionId;
        return $this;
    }


}

