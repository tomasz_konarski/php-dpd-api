<?php

namespace T3ko\Dpd\Soap\Types;

class SchemaValidationException
{

    /**
     * @var string
     */
    private $message;

    /**
     * @return string
     */
    public function getMessage() 
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage($message) 
    {
        $this->message = $message;
        return $this;
    }


}

