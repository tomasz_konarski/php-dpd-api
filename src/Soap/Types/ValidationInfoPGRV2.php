<?php

namespace T3ko\Dpd\Soap\Types;

class ValidationInfoPGRV2
{

    /**
     * @var int
     */
    private $errorId;

    /**
     * @var string
     */
    private $errorCode;

    /**
     * @var string
     */
    private $fieldNames;

    /**
     * @var string
     */
    private $info;

    /**
     * @return int
     */
    public function getErrorId() 
    {
        return $this->errorId;
    }

    /**
     * @param int $errorId
     * @return $this
     */
    public function setErrorId($errorId) 
    {
        $this->errorId = $errorId;
        return $this;
    }

    /**
     * @return string
     */
    public function getErrorCode() 
    {
        return $this->errorCode;
    }

    /**
     * @param string $errorCode
     * @return $this
     */
    public function setErrorCode($errorCode) 
    {
        $this->errorCode = $errorCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getFieldNames() 
    {
        return $this->fieldNames;
    }

    /**
     * @param string $fieldNames
     * @return $this
     */
    public function setFieldNames($fieldNames) 
    {
        $this->fieldNames = $fieldNames;
        return $this;
    }

    /**
     * @return string
     */
    public function getInfo() 
    {
        return $this->info;
    }

    /**
     * @param string $info
     * @return $this
     */
    public function setInfo($info) 
    {
        $this->info = $info;
        return $this;
    }


}

