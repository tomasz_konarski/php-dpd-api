<?php

namespace T3ko\Dpd\Soap\Types;

class InvalidFieldPGRV1
{

    /**
     * @var string
     */
    private $fieldName;

    /**
     * @var string
     */
    private $info;

    /**
     * @var FieldValidationStatusPGREnumV1
     */
    private $status;

    /**
     * @return string
     */
    public function getFieldName() 
    {
        return $this->fieldName;
    }

    /**
     * @param string $fieldName
     * @return $this
     */
    public function setFieldName($fieldName) 
    {
        $this->fieldName = $fieldName;
        return $this;
    }

    /**
     * @return string
     */
    public function getInfo() 
    {
        return $this->info;
    }

    /**
     * @param string $info
     * @return $this
     */
    public function setInfo($info) 
    {
        $this->info = $info;
        return $this;
    }

    /**
     * @return FieldValidationStatusPGREnumV1
     */
    public function getStatus() 
    {
        return $this->status;
    }

    /**
     * @param FieldValidationStatusPGREnumV1 $status
     * @return $this
     */
    public function setStatus(FieldValidationStatusPGREnumV1 $status) 
    {
        $this->status = $status;
        return $this;
    }


}

