<?php

namespace T3ko\Dpd\Soap\Types;

class ProtocolDPPV1
{

    /**
     * @var string
     */
    private $documentId;

    /**
     * @return string
     */
    public function getDocumentId() 
    {
        return $this->documentId;
    }

    /**
     * @param string $documentId
     * @return $this
     */
    public function setDocumentId($documentId) 
    {
        $this->documentId = $documentId;
        return $this;
    }


}

