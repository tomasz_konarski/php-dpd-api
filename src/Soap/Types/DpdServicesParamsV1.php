<?php

namespace T3ko\Dpd\Soap\Types;

class DpdServicesParamsV1
{
    /**
     * @var string
     */
    private $documentId;

    /**
     * @var PickupAddressDSPV1
     */
    private $pickupAddress;

    /**
     * @var PolicyDSPEnumV1
     */
    private $policy;

    /**
     * @var SessionDSPV1
     */
    private $session;

    /**
     * @return string
     */
    public function getDocumentId() 
    {
        return $this->documentId;
    }

    /**
     * @param string $documentId
     *
     * @return $this
     */
    public function setDocumentId($documentId) 
    {
        $this->documentId = $documentId;

        return $this;
    }

    /**
     * @return PickupAddressDSPV1
     */
    public function getPickupAddress() 
    {
        return $this->pickupAddress;
    }

    /**
     * @param PickupAddressDSPV1 $pickupAddress
     *
     * @return $this
     */
    public function setPickupAddress(PickupAddressDSPV1 $pickupAddress) 
    {
        $this->pickupAddress = $pickupAddress;

        return $this;
    }

    /**
     * @return PolicyDSPEnumV1
     */
    public function getPolicy() 
    {
        return $this->policy;
    }

    /**
     * @param PolicyDSPEnumV1 $policy
     *
     * @return $this
     */
    public function setPolicy(PolicyDSPEnumV1 $policy) 
    {
        $this->policy = $policy;

        return $this;
    }

    /**
     * @return SessionDSPV1
     */
    public function getSession() 
    {
        return $this->session;
    }

    /**
     * @param SessionDSPV1 $session
     *
     * @return $this
     */
    public function setSession(SessionDSPV1 $session) 
    {
        $this->session = $session;

        return $this;
    }
}
