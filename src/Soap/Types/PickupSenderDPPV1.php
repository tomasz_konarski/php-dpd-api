<?php

namespace T3ko\Dpd\Soap\Types;

class PickupSenderDPPV1
{

    /**
     * @var string
     */
    private $senderAddress;

    /**
     * @var string
     */
    private $senderCity;

    /**
     * @var string
     */
    private $senderFullName;

    /**
     * @var string
     */
    private $senderName;

    /**
     * @var string
     */
    private $senderPhone;

    /**
     * @var string
     */
    private $senderPostalCode;

    /**
     * @return string
     */
    public function getSenderAddress() 
    {
        return $this->senderAddress;
    }

    /**
     * @param string $senderAddress
     * @return $this
     */
    public function setSenderAddress($senderAddress) 
    {
        $this->senderAddress = $senderAddress;
        return $this;
    }

    /**
     * @return string
     */
    public function getSenderCity() 
    {
        return $this->senderCity;
    }

    /**
     * @param string $senderCity
     * @return $this
     */
    public function setSenderCity($senderCity) 
    {
        $this->senderCity = $senderCity;
        return $this;
    }

    /**
     * @return string
     */
    public function getSenderFullName() 
    {
        return $this->senderFullName;
    }

    /**
     * @param string $senderFullName
     * @return $this
     */
    public function setSenderFullName($senderFullName) 
    {
        $this->senderFullName = $senderFullName;
        return $this;
    }

    /**
     * @return string
     */
    public function getSenderName() 
    {
        return $this->senderName;
    }

    /**
     * @param string $senderName
     * @return $this
     */
    public function setSenderName($senderName) 
    {
        $this->senderName = $senderName;
        return $this;
    }

    /**
     * @return string
     */
    public function getSenderPhone() 
    {
        return $this->senderPhone;
    }

    /**
     * @param string $senderPhone
     * @return $this
     */
    public function setSenderPhone($senderPhone) 
    {
        $this->senderPhone = $senderPhone;
        return $this;
    }

    /**
     * @return string
     */
    public function getSenderPostalCode() 
    {
        return $this->senderPostalCode;
    }

    /**
     * @param string $senderPostalCode
     * @return $this
     */
    public function setSenderPostalCode($senderPostalCode) 
    {
        $this->senderPostalCode = $senderPostalCode;
        return $this;
    }


}

