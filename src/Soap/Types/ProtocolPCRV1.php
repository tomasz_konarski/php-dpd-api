<?php

namespace T3ko\Dpd\Soap\Types;

class ProtocolPCRV1
{

    /**
     * @var string
     */
    private $documentId;

    /**
     * @var StatusInfoPCRV1
     */
    private $statusInfo;

    /**
     * @return string
     */
    public function getDocumentId() 
    {
        return $this->documentId;
    }

    /**
     * @param string $documentId
     * @return $this
     */
    public function setDocumentId($documentId) 
    {
        $this->documentId = $documentId;
        return $this;
    }

    /**
     * @return StatusInfoPCRV1
     */
    public function getStatusInfo() 
    {
        return $this->statusInfo;
    }

    /**
     * @param StatusInfoPCRV1 $statusInfo
     * @return $this
     */
    public function setStatusInfo(StatusInfoPCRV1 $statusInfo) 
    {
        $this->statusInfo = $statusInfo;
        return $this;
    }


}

