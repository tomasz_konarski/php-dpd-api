<?php

namespace T3ko\Dpd\Soap\Types;

class DpdPickupCallParamsV2
{
    /**
     * @var PickupCallOperationTypeDPPEnumV1
     */
    private $operationType;

    /**
     * @var string
     */
    private $orderNumber;

    /**
     * @var PickupCallOrderTypeDPPEnumV1
     */
    private $orderType;

    /**
     * @var PickupCallSimplifiedDetailsDPPV1
     */
    private $pickupCallSimplifiedDetails;

    /**
     * @var string
     */
    private $pickupDate;

    /**
     * @var string
     */
    private $pickupTimeFrom;

    /**
     * @var string
     */
    private $pickupTimeTo;

    /**
     * @var PickupCallUpdateModeDPPEnumV1
     */
    private $updateMode;

    /**
     * @var bool
     */
    private $waybillsReady;

    /**
     * @return PickupCallOperationTypeDPPEnumV1
     */
    public function getOperationType() 
    {
        return $this->operationType;
    }

    /**
     * @param PickupCallOperationTypeDPPEnumV1 $operationType
     *
     * @return $this
     */
    public function setOperationType(PickupCallOperationTypeDPPEnumV1 $operationType) 
    {
        $this->operationType = $operationType;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrderNumber() 
    {
        return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     *
     * @return $this
     */
    public function setOrderNumber($orderNumber) 
    {
        $this->orderNumber = $orderNumber;

        return $this;
    }

    /**
     * @return PickupCallOrderTypeDPPEnumV1
     */
    public function getOrderType() 
    {
        return $this->orderType;
    }

    /**
     * @param PickupCallOrderTypeDPPEnumV1 $orderType
     *
     * @return $this
     */
    public function setOrderType(PickupCallOrderTypeDPPEnumV1 $orderType) 
    {
        $this->orderType = $orderType;

        return $this;
    }

    /**
     * @return PickupCallSimplifiedDetailsDPPV1
     */
    public function getPickupCallSimplifiedDetails() 
    {
        return $this->pickupCallSimplifiedDetails;
    }

    /**
     * @param PickupCallSimplifiedDetailsDPPV1 $pickupCallSimplifiedDetails
     *
     * @return $this
     */
    public function setPickupCallSimplifiedDetails(PickupCallSimplifiedDetailsDPPV1 $pickupCallSimplifiedDetails) 
    {
        $this->pickupCallSimplifiedDetails = $pickupCallSimplifiedDetails;

        return $this;
    }

    /**
     * @return string
     */
    public function getPickupDate() 
    {
        return $this->pickupDate;
    }

    /**
     * @param string $pickupDate
     *
     * @return $this
     */
    public function setPickupDate($pickupDate) 
    {
        $this->pickupDate = $pickupDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getPickupTimeFrom() 
    {
        return $this->pickupTimeFrom;
    }

    /**
     * @param string $pickupTimeFrom
     *
     * @return $this
     */
    public function setPickupTimeFrom($pickupTimeFrom) 
    {
        $this->pickupTimeFrom = $pickupTimeFrom;

        return $this;
    }

    /**
     * @return string
     */
    public function getPickupTimeTo() 
    {
        return $this->pickupTimeTo;
    }

    /**
     * @param string $pickupTimeTo
     *
     * @return $this
     */
    public function setPickupTimeTo($pickupTimeTo) 
    {
        $this->pickupTimeTo = $pickupTimeTo;

        return $this;
    }

    /**
     * @return PickupCallUpdateModeDPPEnumV1
     */
    public function getUpdateMode() 
    {
        return $this->updateMode;
    }

    /**
     * @param PickupCallUpdateModeDPPEnumV1 $updateMode
     *
     * @return $this
     */
    public function setUpdateMode(PickupCallUpdateModeDPPEnumV1 $updateMode) 
    {
        $this->updateMode = $updateMode;

        return $this;
    }

    /**
     * @return bool
     */
    public function isWaybillsReady() 
    {
        return $this->waybillsReady;
    }

    /**
     * @param bool $waybillsReady
     *
     * @return $this
     */
    public function setWaybillsReady($waybillsReady) 
    {
        $this->waybillsReady = $waybillsReady;

        return $this;
    }
}
