<?php

namespace T3ko\Dpd\Soap\Types;

class SessionPGRV2
{

    /**
     * @var string
     */
    private $Status;

    /**
     * @var int
     */
    private $SessionId;

    /**
     * @var \DateTime
     */
    private $BeginTime;

    /**
     * @var \DateTime
     */
    private $EndTime;

    /**
     * @var Packages
     */
    public $Packages;

    /**
     * @return string
     */
    public function getStatus() 
    {
        return $this->Status;
    }

    /**
     * @param string $Status
     * @return $this
     */
    public function setStatus($Status) 
    {
        $this->Status = $Status;
        return $this;
    }

    /**
     * @return int
     */
    public function getSessionId() 
    {
        return $this->SessionId;
    }

    /**
     * @param int $SessionId
     * @return $this
     */
    public function setSessionId($SessionId) 
    {
        $this->SessionId = $SessionId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBeginTime() : \DateTime
    {
        return $this->BeginTime;
    }

    /**
     * @param \DateTime $BeginTime
     * @return $this
     */
    public function setBeginTime($BeginTime) 
    {
        $this->BeginTime = $BeginTime;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndTime() : \DateTime
    {
        return $this->EndTime;
    }

    /**
     * @param \DateTime $EndTime
     * @return $this
     */
    public function setEndTime($EndTime) 
    {
        $this->EndTime = $EndTime;
        return $this;
    }

    /**
     * @return array
     */
    public function getPackages() 
    {
        return $this->Packages;
    }

    /**
     * @param PackagePGRV2[] $Packages
     * @return $this
     */
    public function setPackages(array $Packages) 
    {
        $this->Packages = $Packages;
        return $this;
    }


}

