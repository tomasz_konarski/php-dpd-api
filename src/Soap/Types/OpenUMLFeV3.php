<?php

namespace T3ko\Dpd\Soap\Types;

class OpenUMLFeV3
{

    /**
     * @var PackageOpenUMLFeV3
     */
    private $packages;

    /**
     * @return PackageOpenUMLFeV3
     */
    public function getPackages() 
    {
        return $this->packages;
    }

    /**
     * @param PackageOpenUMLFeV3[] $packages
     * @return $this
     */
    public function setPackages(array $packages) 
    {
        $this->packages = $packages;
        return $this;
    }


}

