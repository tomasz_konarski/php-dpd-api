<?php

namespace T3ko\Dpd\Soap\Types;

class ServicesOpenUMLFeV4
{

    /**
     * @var ServiceCarryInOpenUMLFeV1
     */
    private $carryIn;

    /**
     * @var ServiceCODOpenUMLFeV1
     */
    private $cod;

    /**
     * @var ServiceCUDOpenUMLeFV1
     */
    private $cud;

    /**
     * @var ServiceDeclaredValueOpenUMLFeV1
     */
    private $declaredValue;

    /**
     * @var ServiceDedicatedDeliveryOpenUMLFeV1
     */
    private $dedicatedDelivery;

    /**
     * @var ServiceFlagOpenUMLF
     */
    private $documentsInternational;

    /**
     * @var ServicePalletOpenUMLFeV1
     */
    private $dox;

    /**
     * @var ServiceFlagOpenUMLF
     */
    private $dpdExpress;

    /**
     * @var ServiceDpdPickupOpenUMLFeV1
     */
    private $dpdPickup;

    /**
     * @var ServiceDutyOpenUMLeFV2
     */
    private $duty;

    /**
     * @var ServiceGuaranteeOpenUMLFeV1
     */
    private $guarantee;

    /**
     * @var ServiceInPersOpenUMLFeV1
     */
    private $inPers;

    /**
     * @var ServicePalletOpenUMLFeV1
     */
    private $pallet;

    /**
     * @var ServicePrivPersOpenUMLFeV1
     */
    private $privPers;

    /**
     * @var ServiceRODOpenUMLFeV1
     */
    private $rod;

    /**
     * @var ServiceSelfColOpenUMLFeV1
     */
    private $selfCol;

    /**
     * @var ServiceTiresOpenUMLFeV1
     */
    private $tires;

    /**
     * @var ServiceTiresExportOpenUMLFeV1
     */
    private $tiresExport;

    /**
     * @return ServiceCarryInOpenUMLFeV1
     */
    public function getCarryIn() 
    {
        return $this->carryIn;
    }

    /**
     * @param ServiceCarryInOpenUMLFeV1 $carryIn
     * @return $this
     */
    public function setCarryIn(ServiceCarryInOpenUMLFeV1 $carryIn) 
    {
        $this->carryIn = $carryIn;
        return $this;
    }

    /**
     * @return ServiceCODOpenUMLFeV1
     */
    public function getCod() 
    {
        return $this->cod;
    }

    /**
     * @param ServiceCODOpenUMLFeV1 $cod
     * @return $this
     */
    public function setCod(ServiceCODOpenUMLFeV1 $cod) 
    {
        $this->cod = $cod;
        return $this;
    }

    /**
     * @return ServiceCUDOpenUMLeFV1
     */
    public function getCud() 
    {
        return $this->cud;
    }

    /**
     * @param ServiceCUDOpenUMLeFV1 $cud
     * @return $this
     */
    public function setCud(ServiceCUDOpenUMLeFV1 $cud) 
    {
        $this->cud = $cud;
        return $this;
    }

    /**
     * @return ServiceDeclaredValueOpenUMLFeV1
     */
    public function getDeclaredValue() 
    {
        return $this->declaredValue;
    }

    /**
     * @param ServiceDeclaredValueOpenUMLFeV1 $declaredValue
     * @return $this
     */
    public function setDeclaredValue(ServiceDeclaredValueOpenUMLFeV1 $declaredValue) 
    {
        $this->declaredValue = $declaredValue;
        return $this;
    }

    /**
     * @return ServiceDedicatedDeliveryOpenUMLFeV1
     */
    public function getDedicatedDelivery() 
    {
        return $this->dedicatedDelivery;
    }

    /**
     * @param ServiceDedicatedDeliveryOpenUMLFeV1 $dedicatedDelivery
     * @return $this
     */
    public function setDedicatedDelivery(ServiceDedicatedDeliveryOpenUMLFeV1 $dedicatedDelivery) 
    {
        $this->dedicatedDelivery = $dedicatedDelivery;
        return $this;
    }

    /**
     * @return ServiceFlagOpenUMLF
     */
    public function getDocumentsInternational() 
    {
        return $this->documentsInternational;
    }

    /**
     * @param ServiceFlagOpenUMLF $documentsInternational
     * @return $this
     */
    public function setDocumentsInternational(ServiceFlagOpenUMLF $documentsInternational) 
    {
        $this->documentsInternational = $documentsInternational;
        return $this;
    }

    /**
     * @return ServicePalletOpenUMLFeV1
     */
    public function getDox() 
    {
        return $this->dox;
    }

    /**
     * @param ServicePalletOpenUMLFeV1 $dox
     * @return $this
     */
    public function setDox(ServicePalletOpenUMLFeV1 $dox) 
    {
        $this->dox = $dox;
        return $this;
    }

    /**
     * @return ServiceFlagOpenUMLF
     */
    public function getDpdExpress() 
    {
        return $this->dpdExpress;
    }

    /**
     * @param ServiceFlagOpenUMLF $dpdExpress
     * @return $this
     */
    public function setDpdExpress(ServiceFlagOpenUMLF $dpdExpress) 
    {
        $this->dpdExpress = $dpdExpress;
        return $this;
    }

    /**
     * @return ServiceDpdPickupOpenUMLFeV1
     */
    public function getDpdPickup() 
    {
        return $this->dpdPickup;
    }

    /**
     * @param ServiceDpdPickupOpenUMLFeV1 $dpdPickup
     * @return $this
     */
    public function setDpdPickup(ServiceDpdPickupOpenUMLFeV1 $dpdPickup) 
    {
        $this->dpdPickup = $dpdPickup;
        return $this;
    }

    /**
     * @return ServiceDutyOpenUMLeFV2
     */
    public function getDuty() 
    {
        return $this->duty;
    }

    /**
     * @param ServiceDutyOpenUMLeFV2 $duty
     * @return $this
     */
    public function setDuty(ServiceDutyOpenUMLeFV2 $duty) 
    {
        $this->duty = $duty;
        return $this;
    }

    /**
     * @return ServiceGuaranteeOpenUMLFeV1
     */
    public function getGuarantee() 
    {
        return $this->guarantee;
    }

    /**
     * @param ServiceGuaranteeOpenUMLFeV1 $guarantee
     * @return $this
     */
    public function setGuarantee(ServiceGuaranteeOpenUMLFeV1 $guarantee) 
    {
        $this->guarantee = $guarantee;
        return $this;
    }

    /**
     * @return ServiceInPersOpenUMLFeV1
     */
    public function getInPers() 
    {
        return $this->inPers;
    }

    /**
     * @param ServiceInPersOpenUMLFeV1 $inPers
     * @return $this
     */
    public function setInPers(ServiceInPersOpenUMLFeV1 $inPers) 
    {
        $this->inPers = $inPers;
        return $this;
    }

    /**
     * @return ServicePalletOpenUMLFeV1
     */
    public function getPallet() 
    {
        return $this->pallet;
    }

    /**
     * @param ServicePalletOpenUMLFeV1 $pallet
     * @return $this
     */
    public function setPallet(ServicePalletOpenUMLFeV1 $pallet) 
    {
        $this->pallet = $pallet;
        return $this;
    }

    /**
     * @return ServicePrivPersOpenUMLFeV1
     */
    public function getPrivPers() 
    {
        return $this->privPers;
    }

    /**
     * @param ServicePrivPersOpenUMLFeV1 $privPers
     * @return $this
     */
    public function setPrivPers(ServicePrivPersOpenUMLFeV1 $privPers) 
    {
        $this->privPers = $privPers;
        return $this;
    }

    /**
     * @return ServiceRODOpenUMLFeV1
     */
    public function getRod() 
    {
        return $this->rod;
    }

    /**
     * @param ServiceRODOpenUMLFeV1 $rod
     * @return $this
     */
    public function setRod(ServiceRODOpenUMLFeV1 $rod) 
    {
        $this->rod = $rod;
        return $this;
    }

    /**
     * @return ServiceSelfColOpenUMLFeV1
     */
    public function getSelfCol() 
    {
        return $this->selfCol;
    }

    /**
     * @param ServiceSelfColOpenUMLFeV1 $selfCol
     * @return $this
     */
    public function setSelfCol(ServiceSelfColOpenUMLFeV1 $selfCol) 
    {
        $this->selfCol = $selfCol;
        return $this;
    }

    /**
     * @return ServiceTiresOpenUMLFeV1
     */
    public function getTires() 
    {
        return $this->tires;
    }

    /**
     * @param ServiceTiresOpenUMLFeV1 $tires
     * @return $this
     */
    public function setTires(ServiceTiresOpenUMLFeV1 $tires) 
    {
        $this->tires = $tires;
        return $this;
    }

    /**
     * @return ServiceTiresExportOpenUMLFeV1
     */
    public function getTiresExport() 
    {
        return $this->tiresExport;
    }

    /**
     * @param ServiceTiresExportOpenUMLFeV1 $tiresExport
     * @return $this
     */
    public function setTiresExport(ServiceTiresExportOpenUMLFeV1 $tiresExport) 
    {
        $this->tiresExport = $tiresExport;
        return $this;
    }


}

