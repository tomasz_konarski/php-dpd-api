<?php

namespace T3ko\Dpd\Soap\Types;

class PickupCustomerDPPV1
{

    /**
     * @var string
     */
    private $customerFullName;

    /**
     * @var string
     */
    private $customerName;

    /**
     * @var string
     */
    private $customerPhone;

    /**
     * @return string
     */
    public function getCustomerFullName() 
    {
        return $this->customerFullName;
    }

    /**
     * @param string $customerFullName
     * @return $this
     */
    public function setCustomerFullName($customerFullName) 
    {
        $this->customerFullName = $customerFullName;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerName() 
    {
        return $this->customerName;
    }

    /**
     * @param string $customerName
     * @return $this
     */
    public function setCustomerName($customerName) 
    {
        $this->customerName = $customerName;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerPhone() 
    {
        return $this->customerPhone;
    }

    /**
     * @param string $customerPhone
     * @return $this
     */
    public function setCustomerPhone($customerPhone) 
    {
        $this->customerPhone = $customerPhone;
        return $this;
    }


}

