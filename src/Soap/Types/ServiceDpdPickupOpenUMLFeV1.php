<?php

namespace T3ko\Dpd\Soap\Types;

class ServiceDpdPickupOpenUMLFeV1
{

    /**
     * @var string
     */
    private $pudo;

    /**
     * @return string
     */
    public function getPudo() 
    {
        return $this->pudo;
    }

    /**
     * @param string $pudo
     * @return $this
     */
    public function setPudo($pudo) 
    {
        $this->pudo = $pudo;
        return $this;
    }


}

