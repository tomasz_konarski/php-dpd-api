<?php

namespace T3ko\Dpd\Soap\Types;

class InvalidFieldPAV1
{

    /**
     * @var string
     */
    private $fieldName;

    /**
     * @var string
     */
    private $info;

    /**
     * @var string
     */
    private $status;

    /**
     * @return string
     */
    public function getFieldName() 
    {
        return $this->fieldName;
    }

    /**
     * @param string $fieldName
     * @return $this
     */
    public function setFieldName($fieldName) 
    {
        $this->fieldName = $fieldName;
        return $this;
    }

    /**
     * @return string
     */
    public function getInfo() 
    {
        return $this->info;
    }

    /**
     * @param string $info
     * @return $this
     */
    public function setInfo($info) 
    {
        $this->info = $info;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus() 
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status) 
    {
        $this->status = $status;
        return $this;
    }


}

