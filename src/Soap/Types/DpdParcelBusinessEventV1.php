<?php

namespace T3ko\Dpd\Soap\Types;

class DpdParcelBusinessEventV1
{
    /**
     * @var string
     */
    private $countryCode;

    /**
     * @var string
     */
    private $eventCode;

    /**
     * @var DpdParcelBusinessEventDataV1
     */
    private $eventDataList;

    /**
     * @var \DateTime
     */
    private $eventTime;

    /**
     * @var string
     */
    private $postalCode;

    /**
     * @var string
     */
    private $waybill;

    /**
     * @return string
     */
    public function getCountryCode() 
    {
        return $this->countryCode;
    }

    /**
     * @param string $countryCode
     *
     * @return $this
     */
    public function setCountryCode($countryCode) 
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getEventCode() 
    {
        return $this->eventCode;
    }

    /**
     * @param string $eventCode
     *
     * @return $this
     */
    public function setEventCode($eventCode) 
    {
        $this->eventCode = $eventCode;

        return $this;
    }

    /**
     * @return DpdParcelBusinessEventDataV1
     */
    public function getEventDataList() 
    {
        return $this->eventDataList;
    }

    /**
     * @param DpdParcelBusinessEventDataV1 $eventDataList
     *
     * @return $this
     */
    public function setEventDataList(DpdParcelBusinessEventDataV1 $eventDataList) 
    {
        $this->eventDataList = $eventDataList;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEventTime() : \DateTime
    {
        return $this->eventTime;
    }

    /**
     * @param \DateTime $eventTime
     *
     * @return $this
     */
    public function setEventTime($eventTime) 
    {
        $this->eventTime = $eventTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getPostalCode() 
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     *
     * @return $this
     */
    public function setPostalCode($postalCode) 
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getWaybill() 
    {
        return $this->waybill;
    }

    /**
     * @param string $waybill
     *
     * @return $this
     */
    public function setWaybill($waybill) 
    {
        $this->waybill = $waybill;

        return $this;
    }
}
