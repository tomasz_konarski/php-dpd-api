<?php

namespace T3ko\Dpd\Soap\Types;

class FindPostalCodeResponseV1
{
    /**
     * @var string
     */
    private $status;

    /**
     * @return string
     */
    public function getStatus() 
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return $this
     */
    public function setStatus($status) 
    {
        $this->status = $status;

        return $this;
    }
}
