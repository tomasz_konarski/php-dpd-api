<?php

namespace T3ko\Dpd\Soap\Types;

class AuthDataV1
{
    /**
     * @var string
     */
    private $login;

    /**
     * @var int
     */
    private $masterFid;

    /**
     * @var string
     */
    private $password;

    /**
     * @return string
     */
    public function getLogin() 
    {
        return $this->login;
    }

    /**
     * @param string $login
     *
     * @return $this
     */
    public function setLogin($login) 
    {
        $this->login = $login;

        return $this;
    }

    /**
     * @return int
     */
    public function getMasterFid() 
    {
        return $this->masterFid;
    }

    /**
     * @param int $masterFid
     *
     * @return $this
     */
    public function setMasterFid($masterFid) 
    {
        $this->masterFid = $masterFid;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword() 
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return $this
     */
    public function setPassword($password) 
    {
        $this->password = $password;

        return $this;
    }
}
