<?php

namespace T3ko\Dpd\Soap\Types;

class DestinationsData
{
    /**
     * @var string
     */
    private $destinationName;

    /**
     * @var string
     */
    private $documentId;

    /**
     * @var bool
     */
    private $domestic;

    /**
     * @return string
     */
    public function getDestinationName() 
    {
        return $this->destinationName;
    }

    /**
     * @param string $destinationName
     *
     * @return $this
     */
    public function setDestinationName($destinationName) 
    {
        $this->destinationName = $destinationName;

        return $this;
    }

    /**
     * @return string
     */
    public function getDocumentId() 
    {
        return $this->documentId;
    }

    /**
     * @param string $documentId
     *
     * @return $this
     */
    public function setDocumentId($documentId) 
    {
        $this->documentId = $documentId;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDomestic() 
    {
        return $this->domestic;
    }

    /**
     * @param bool $domestic
     *
     * @return $this
     */
    public function setDomestic($domestic) 
    {
        $this->domestic = $domestic;

        return $this;
    }
}
